package second;


public class Bird extends Fly{
	private String bird;

	public Bird(String bird){
		super(bird);
		this.bird = bird;
	}
	
	public void flying(){
		System.out.println(bird);
	}

	public void iFly(){
		System.out.println(bird);
	}
}
