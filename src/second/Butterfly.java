package second;


public class Butterfly extends Fly{
	private String butterfly;
	
	public Butterfly(String butterfly){
		super(butterfly);
		this.butterfly = butterfly;
	}
	
	public void flying(){
		System.out.println(butterfly);
	}
	
	public void iFly(){
		System.out.println(butterfly);
	}

}
