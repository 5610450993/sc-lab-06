package second;


public abstract class Fly {
	public Fly(String animal){
		System.out.println(animal+" Is Flying");
	}
	
	public abstract void flying();
}
