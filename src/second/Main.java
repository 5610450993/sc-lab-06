package second;


public class Main {
	
	public static void main(String[] args) {
		Bird Bird = new Bird("Bird Fly");
		Butterfly but = new Butterfly("Butterfly Fly");
		
		Bird.flying();
		but.flying();
		
		Bird.iFly();
		but.iFly();
	}

}
