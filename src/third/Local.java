package third;


public class Local {
	private String text = "non local method";
	private String text2 = "private: call class";
	public String text3 = "public: call class";
	protected String text4 = "protected: call class";

	public void showText1(String text){
		System.out.println(this.text);
	}
	
	public void showText2(String text){
		System.out.println(text);
	}
	
	
	public static void main(String[] args) {
		//3.1
		String text = "local method";
		new Local().showText1(text);
		new Local().showText2(text);
		
		//3.2
		System.out.println("\n<----------Call Class---------->");
		System.out.println(new Local().text2);
		System.out.println(new Local().text3);
		System.out.println(new Local().text4);
		System.out.println("<----------Call Package---------->");
		System.out.println(new Call().getText2());
		System.out.println(new Call().text3);
		System.out.println(new Call().text4);

	}

}
